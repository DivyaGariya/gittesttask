import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import ProductList from './container/productList';
import HomePage from './container/home';
import ProductDetails from './components/productDetails';
import Checkout from './components/checkout';

class App extends Component {
  render() {
      return (
        <Router>
              <Switch>
                <Route exact path='/' component={HomePage} exact />
                  <Route path='/ProductList' component={ProductList} />
                  <Route path='/ProductDetails' component={ProductDetails} />
                  <Route path='/Checkout' component={Checkout} />
          </Switch>
    </Router>
    );
  }
}

export default App;