import * as types from '../constants';
import { auth } from '../axios';
import { pass, say } from '../axios/actions'

export const productAction = () => async(dispatch) => {
        await auth.get('products').then(res => {
            const data = res.data;
            console.log('data testing products',data)
            dispatch(pass(types.GET_DATA_SUCCESS,data))
        }).catch(err => {console.log('Get error',err) });
}

export const signleProductAction = () => async(dispatch) => {
    await auth.get('products/category/jewelery').then(res => {
        const data = res.data;
        console.log('data testing products',data)
        dispatch(pass(types.GET_SINGLE_SUCCESS,data))
    }).catch(err => {console.log('Get error',err) });
}


