import axios from 'axios';
import * as constants from '../constants';

// Axios Instance for register & login
export let auth = axios.create({
  baseURL: constants.AUTH_URL,
  headers: {
    'Accept': 'application/json',
    // 'Content-Type': 'application/x-www-form-urlencoded',
  },
  timeout: 10000,
  responseType: 'json',
});

export let authSearch = axios.create({
  baseURL: constants.AUTH_URL,
  headers:{
    'Accept': 'application/json'
  }
})
