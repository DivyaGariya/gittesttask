import React, { Component } from "react";
import { Link } from "react-router-dom";
import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";
import "../styles/slider.css";
import { products, images } from "../dataStore";

var imgSlides = () =>
  images.map((num) => (
    <div className="imgpad">
      <img className="imgdetails" src={num.img} width="100%" />
    </div>
  ));
const Banner = () => {
  return (
    <div class="sliderContainer">
      <div class="col-sm-12 sliderBox">
        <Slider
          dots={true}
          slidesToShow={2}
          slidesToScroll={2}
          autoplay={true}
          arrows={true}
          autoplaySpeed={5000}
        >
          {imgSlides()}
        </Slider>
      </div>
    </div>
  );
};

export default Banner;
