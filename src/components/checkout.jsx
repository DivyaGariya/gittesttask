import React, { useState } from "react";
import Header from "../common/head";

export default () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [contact, setContact] = useState('');
  const [coupon, setCoupon] = useState('');
  const [address, setAddress] = useState('');

  const handleChangeName = (e) => {
    setName(e.target.value)
  }
  const handleChangeEmail = (e) => {
    setEmail(e.target.value)
  }
  const handleChangeAddress = (e) => {
    setAddress(e.target.value)
  }
  const handleChangeContact = (e) => {
    setContact(e.target.value)
  }
  const handleChangeCoupon = (e) => {
    setCoupon(e.target.value)
  }
  const submit = () => {
    if (name !==''&& email!=='' && contact  !==''&& coupon !=='' && address !=='') {
      alert('Order Place successfully')
    } else {
      alert('Please fill all Details')
    }
  }
  return (
    <div class="panel panel-info">
      <Header />
      <div class="panel-body" style={{marginTop:10}}>
        <div class="form-group">
          <div class="col-md-12">
            <h4>Shipping Address</h4>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-6 col-xs-12">
            <strong>Name:</strong>
            <input
              type="text"
              name="first_name"
              class="form-control"
              value={name}
              onChange={handleChangeName}
            />
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <strong>Email Address:</strong>
          </div>
          <div class="col-md-12">
            <input
              type="text"
              name="email_address"
              class="form-control"
              value={email}
              onChange={handleChangeEmail}
            />
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <strong>Phone Number:</strong>
          </div>
          <div class="col-md-12">
            <input
              type="text"
              name="phone_number"
              class="form-control"
              value={contact}
              onChange={handleChangeContact}
            />
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <strong>Address:</strong>
          </div>
          <div class="col-md-12">
            <input type="text" name="address" class="form-control" value={address}  onChange={handleChangeAddress}/>
          </div>
        </div>
        <div class="form-group">
          <div class="col-md-12">
            <strong>Coupon code:</strong>
          </div>
          <div class="col-md-12">
            <input type="text" name="state" class="form-control" value={coupon} onChange={handleChangeCoupon}/>
          </div>
        </div>
      </div>
      <div class="col-md-12 " className="checkoutBtn">
        <button
          class="btn btn-primary center-block btn-md"
          onClick={() => submit()}
        >
          Checkout
        </button>
      </div>
    </div>
  );
};
