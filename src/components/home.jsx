import React, { Component, useEffect } from "react";
import { Link } from "react-router-dom";
import "../styles/home.css";
import { products, images } from "../dataStore";
import Header from "../common/head";
import Banner from "../common/slider";

export default ({ actions, ProductReducer }) => {
  let data = ProductReducer;
  const productList = data?.data;

    return (
      <div >
        <Header />
        <Banner />
        <div class="row itemDiv">
          {productList?.map((item, index) => {
            return (
              <figure class="col-md-3" key={index}>
                <Link
                  to={{
                    pathname: '/ProductList', 
                    query:{name: item.title, category: item.category}
                  }}
                  class="card card-product"
                  style={{ textDecoration: 'none' }}
                >
                  <div class="img-wrap">
                    <img src={item.image}/>
                  </div>
                  <figcaption class="info-wrap">
                    <h6 class="title">{item.title}</h6>
                  </figcaption>
                </Link>
              </figure>
            );
          })}
        </div>
      </div>
    );
  }

