import React from "react";
import Header from "../common/head";
import "../styles/productDetails.css";

export default (props) => {
  const { data } = props?.location?.query;
  console.log('datadatadata',data)
   const handlePress = () => {
        window.open("./checkout");
  }
  return (
    <div>
      <Header />
      <div class="container">
        <div class="card ProductDetailsCard">
          <div class="container-fliud">
            <div class="wrapper row">
              <div class="preview col-md-6">
                <div class="preview-pic tab-content">
                  <div class="tab-pane active" id="pic-1">
                    <img src={data?.image} className='customImage' />
                  </div>
                  <div class="tab-pane" id="pic-2">
                    <img src={data?.image} />
                  </div>
                  <div class="tab-pane" id="pic-3">
                    <img src={data?.image} />
                  </div>
                  <div class="tab-pane" id="pic-4">
                    {/* <img src={data?.image} /> */}
                  </div>
                  <div class="tab-pane" id="pic-5">
                    {/* <img src={data?.image} /> */}
                  </div>
                </div>
                <ul class="preview-thumbnail nav nav-tabs">
                  <li class="active">
                    <a data-target="#pic-1" data-toggle="tab">
                      <img src={data?.image} />
                    </a>
                  </li>
                  <li>
                    <a data-target="#pic-2" data-toggle="tab">
                      <img src={data?.image} />
                    </a>
                  </li>
                  <li>
                    <a data-target="#pic-3" data-toggle="tab">
                      <img src={data?.image} />
                    </a>
                  </li>
                  <li>
                    <a data-target="#pic-4" data-toggle="tab">
                      <img src={data?.image} />
                    </a>
                  </li>
                  <li>
                    <a data-target="#pic-5" data-toggle="tab">
                      <img src={data?.image} />
                    </a>
                  </li>
                </ul>
              </div>
              <div class="details col-md-6">
                <h3 class="product-title">
                  {data?.title}
                </h3>
                <div class="rating">
                  <div class="stars">
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star checked"></span>
                    <span class="fa fa-star"></span>
                    <span class="fa fa-star"></span>
                  </div>
                  <span class="review-no">41 reviews</span>
                </div>
                <p class="product-description">
                  Suspendisse quos? Tempus cras iure temporibus? Eu laudantium
                  cubilia sem sem! Repudiandae et! Massa senectus enim minim
                  sociosqu delectus posuere.
                </p>
                <h4 class="price">
                  current price: <span>{data?.price}</span>
                </h4>
                <p class="vote">
                  <strong>91%</strong> of buyers enjoyed this product!{" "}
                  <strong>(87 votes)</strong>
                </p>
                <h5 class="colors">
                  colors:
                  <span
                    class="color black"
                    data-toggle="tooltip"
                    title="Not In store"
                  ></span>
                  <span class="color gray"></span>
                  <span class="color blue"></span>
                </h5>
                <div class="action">
                  <button class="add-to-cart btn btn-default" type="button" onClick={() =>handlePress()}>
                   Buy now
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
