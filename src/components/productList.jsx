import React, { Component, useEffect } from "react";
import { Link, useRouteMatch, useParams, useLocation } from "react-router-dom";
import "./productItem.css";
import Header from "../common/head";


export default ({ actions,SingleProductReducer }) => {
  
  let location = useLocation();
  useEffect(() => {
    actions.signleProductAction();
  }, [])
  const productList = SingleProductReducer?.data;
  console.log('SingleProductReducer',location)

  const { query } = location;
  const name = query?.category ? query?.category : 'Products List'
  return (
    <div>
      <Header />
      <div class="card">
        <div class="card-body">
          <h5 class="card-title">
           {(name).toUpperCase()}
          </h5>
        </div>
      </div>
      <div class="row itemDiv">
        {productList?.map((item, index) => {
          return (
            <figure class="col-md-3 mainbox"  key={index}>
              <Link
                class="card card-product"
                to={{
                  pathname: '/ProductDetails', 
                  query:{data: item}
                }}
                style={{ textDecoration: 'none' }}
              >
                <div class="img-wrap">
                  <img src={item?.image} />
                </div>
                <figcaption class="info-wrap">
                  <h6 class="title">{(item?.title).length > 10 ?item?.title.slice(0,10) :item?.title}</h6>
                </figcaption>
                <div class="bottom-wrap">
                  <div class="price-wrap h5">
                    <span class="price-new cost">{item?.price}$</span>
                  </div>
                </div>
              </Link>
            </figure>
          );
        })}
      </div>
    </div>
  );
};
