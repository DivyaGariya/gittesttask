export const AUTH_URL = 'https://fakestoreapi.com/';

export const GET_DATA_LOADING = 'GET_DATA_LOADING';
export const GET_DATA_SUCCESS = 'GET_DATA_SUCCESS';
export const GET_DATA_ERROR = 'GET_DATA_ERROR';

export const GET_SINGLE_LOADING = 'GET_SINGLE_LOADING';
export const GET_SINGLE_SUCCESS = 'GET_SINGLE_SUCCESS';
export const GET_SINGLE_ERROR = 'GET_SINGLE_ERROR';