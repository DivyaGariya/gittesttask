import React, { useEffect } from "react";
import {productAction} from "../action/index";
import HomePage from '../components/home';  
import { useSelector, useDispatch } from "react-redux";
import { bindActionCreators } from "redux";

export default () => {
  const { ProductReducer } = useSelector((state) => state);
    const actions = bindActionCreators({ productAction }, useDispatch());
    useEffect(() => {
        actions.productAction();
    }, [])
  return (
    <HomePage
      {...{
        actions,
        ProductReducer
      }}
    />
  );
};
