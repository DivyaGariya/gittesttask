import React from "react";
import { increment, decrement } from "../action/index";
import Increment from '../component/increment';
import { useSelector, useDispatch } from "react-redux";
import { bindActionCreators } from "redux";

export default () => {
  const { incrementDecrement } = useSelector((state) => state);
  const actions = bindActionCreators({increment,decrement},useDispatch());
  return (
    <Increment
      {...{
        actions,
        incrementDecrement
      }}
    />
  );
};
