import React, { useEffect } from "react";
import {signleProductAction} from "../action/index";
import ProductListPage from '../components/productList';  
import { useSelector, useDispatch } from "react-redux";
import { bindActionCreators } from "redux";

export default () => {
  const { SingleProductReducer } = useSelector((state) => state);
    const actions = bindActionCreators({ signleProductAction }, useDispatch());
    // useEffect(() => {
    //     actions.signleProductAction();
    // }, [])
  return (
    <ProductListPage
      {...{
        actions,
        SingleProductReducer
      }}
    />
  );
};
