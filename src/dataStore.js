export const products = [
    {
      id: 1,
      name: "Camera",
      price: "200$",
      img:
            "https://asia.canon/media/migration/shared/live/products/EN/eos_m50_m55-200_b1.png",
      nav:'ProductList'
    },
    {
      id: 2,
      name: "Laptop",
      price: "100$",
      img:
        "https://www.pngitem.com/pimgs/m/194-1945689_transparent-laptops-png-laptop-banner-image-in-png.png",
        nav:'ProductList'
    },
    {
      id: 3,
      name: "Mobile",
      price: "300$",
      img:
        "https://developer.apple.com/app-store/marketing/guidelines/images/thumbnail-iphone11_2x.png",
        nav:'ProductList'
    },
    {
      id: 4,
      name: "Earbuds",
      price: "400$",
      img:
        "https://www.jabra.com/-/media/Images/Products/Jabra-Elite-75t/Layout/1_Topbanner/e75_top_product_black.png?la=en&hash=0D9073190BDCC6C048337EBFF6D1FBE3C159F0BC",
        nav:'ProductList'
    },
    {
      id: 5,
      name: "Thermal Imaging",
      price: "400$",
      img:
        "https://www.flir.com/globalassets/imported-assets/image/vue-tz20-3qtrfrtrght-01.png",
        nav:'ProductList'
    },
    {
      id: 6,
      name: "TCL",
      price: "400$",
      img:
        "https://www.tcl.com/usca/content/dam/tcl/product/mobile/tcl-10-l/teaser/10L-720x480.png",
        nav:'ProductList'
    },
    {
      id: 7,
      name: "Aquafina",
      price: "400$",
      img: "https://www.aquafina.com/images/home/aqf-cans.png",
      nav:'ProductList'
    },
    {
      id: 8,
      name: "Foxin",
      price: "400$",
      img:
        "https://www.foxin.in/wp-content/uploads/2019/12/roar-foxin-product.png",
        nav:'ProductList'
    },
];
export const images = [
    {
      img:
        "https://ahoomstore.com/wp-content/uploads/2019/01/bags-banner-png.png",
    },
    {
      img:
        "https://www.pngmart.com/files/7/Baby-Clothes-Transparent-Background.png",
    },
    { img: "https://www.nikon.com/img/index/pic_products_01_l.png" },
    {
      img:
        "https://www.forsalesigns.co.nz/wp-content/uploads/2017/03/clothing-banner.png",
    },
    {
      img:
        "https://www.technocomp.net/salesimg/12HPLaptopBanner.png",
    },
];

export const cameras = [
    {
      id: 1,
      name: "Camera 1",
      price: "200$",
      img:
        "https://lh3.googleusercontent.com/proxy/xo0Rqwe9Kv7kfYlpLVEiGMJZ0rrzuhIJlyHjs5lXiBDJcdd1_oD30fNjj5lfyPyAS9P-3jmTQDC2zgABBUuTgkj171n04hmxJJ-MTSTZtPAQhdaoLnuZtFUZlZVq3DQACkT5lSAu",
    },
    {
      id: 2,
      name: "Camera 2",
      price: "100$",
      img:
        "https://pngimg.com/uploads/photo_camera/photo_camera_PNG7846.png",
    },
    {
      id: 3,
      name: "Camera 3",
      price: "300$",
      img:
        "https://pngimg.com/uploads/photo_camera/photo_camera_PNG7818.png",
    },
    {
      id: 4,
      name: "Camera 4",
      price: "400$",
      img:
        "https://www.pngmart.com/files/6/DSLR-Camera-Transparent-PNG.png",
    },
    {
      id: 5,
      name: "Camera 5",
      price: "400$",
      img:
        "https://www.flir.com/globalassets/imported-assets/image/vue-tz20-3qtrfrtrght-01.png",
    },
    {
      id: 6,
      name: "Camera 6",
      price: "400$",
      img:
        "https://www.pngmart.com/files/2/Canon-Digital-Camera-PNG-File.png",
    },
    {
      id: 7,
      name: "Camera 7",
      price: "400$",
      img: "https://pngimg.com/uploads/video_camera/video_camera_PNG7892.png",
    },
    {
      id: 8,
      name: "Camera 8",
      price: "400$",
      img:
        "https://www.pngmart.com/files/2/Sony-Digital-Camera-PNG-Clipart.png",
    },
];
  
export const laptops = [
    {
      id: 1,
      name: "Laptop 1",
      price: "200$",
      img:
        "https://www.pngmart.com/files/4/Asus-Laptop-PNG-HD.png",
    },
    {
      id: 2,
      name: "Laptop 2",
      price: "100$",
      img:
        "https://www.kindpng.com/picc/m/151-1510007_download-and-use-laptops-png-in-high-resolution.png",
    },
    {
      id: 3,
      name: "Laptop 3",
      price: "300$",
      img:
        "https://www.pngkey.com/png/full/1-19334_free-png-dell-laptop-png-images-transparent-dell.png",
    },
    {
      id: 4,
      name: "Laptop 4",
      price: "400$",
      img:
        "https://f0.pngfuel.com/png/868/313/black-and-gray-laptop-computer-laptop-computer-icons-computer-monitors-laptops-png-clip-art.png",
    },
    {
      id: 5,
      name: "Laptop 5",
      price: "400$",
      img:
        "https://freepngimg.com/thumb/technology/49976-4-dell-laptop-image-free-transparent-image-hq.png",
    },
    {
      id: 6,
      name: "Laptop 6",
      price: "400$",
      img:
        "https://static.acer.com/up/Resource/Acer/Laptops/Nitro_5/Image/20200413/Acer-Nitro-5_AN515-55_modelpreview.png",
    },
    {
      id: 7,
      name: "Laptop 7",
      price: "400$",
      img: "https://betanews.com/wp-content/uploads/2018/05/MateBook-X-Pro_group-1_preview.png",
    },
    {
      id: 8,
      name: "Laptop 8",
      price: "400$",
      img:
        "https://5.imimg.com/data5/TC/DN/MY-45251354/hp-pavilion-x360-13-u131tu-500x500.png",
    },
  ];