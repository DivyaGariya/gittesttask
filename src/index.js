import React from 'react';
import ReactDOM from "react-dom";
import { BrowserRouter } from 'react-router-dom';
import { Provider } from "react-redux";
import { createStore,applyMiddleware } from "redux";
import rootReducer from './store/reducers';
import 'bootstrap/dist/css/bootstrap.css';
import thunk from 'redux-thunk';
import App from './App';

let store = createStore(rootReducer,applyMiddleware(thunk))

ReactDOM.render(
  <Provider store={store}>
    <BrowserRouter>
    <App />
    </BrowserRouter>
  </Provider>,
  document.getElementById('root')
);

