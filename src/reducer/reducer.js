import * as types from '../constants';

const initialState = {
    data:null
};

//Get Cart Data
export const ProductReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.GET_DATA_SUCCESS:
            return Object.assign({}, state, {data:action?.payload});
        }
    return state;
};

export const SingleProductReducer = (state = initialState, action) => {
    switch (action.type) {
        case types.GET_SINGLE_SUCCESS:
            return Object.assign({}, state, {data:action.payload});
        }
    return state;
};