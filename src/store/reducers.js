import {combineReducers} from 'redux';
import {ProductReducer,SingleProductReducer} from '../reducer/reducer'

const appReducer = combineReducers({
    ProductReducer: ProductReducer,
    SingleProductReducer:SingleProductReducer
})
const rootReducer = (state, action) => {
    if (action.type === 'IS_LOGOUT') {
        state = undefined;
    }

	return appReducer(state, action);
};
export default rootReducer;