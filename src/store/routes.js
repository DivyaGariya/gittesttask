import React from 'react';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import Login from '../component/login';
import CountFile from "../container/increment";


const navigation = () =>(
    <Router>
        <div>
            <ul>
                <li>
                    <Link to=''>Home</Link>
                    <Link to=''>About</Link>
                    <Link to=''>Login</Link>
                </li>
            </ul>
            <hr/>
            {/* <Route exact path='/' component={Home}/> */}
            <Route path="/increment" component={CountFile} />
            <Route path="/login" component={Login} />
        </div>
    </Router>
)

export default navigation;